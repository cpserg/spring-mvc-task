package by.itsupportme.news.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import by.itsupportme.news.model.Comment;

/**
 * Comment repository definitions
 * 
 * @author Siarhei Tsytrykau
 *
 */
public interface CommentRepository extends BaseRepository<Comment, Long> {

	/**
	 * Returns a list of Comment for certain News by specified newsId. 
	 * Returned comment list is sorted by descending order.
	 * 
	 * 
	 * @param newsId id of news
	 * @return List<Comment>
	 * @see by.itsupportme.news.model.Comment
	 * @see by.itsupportme.news.model.News
	 */
	@Query(value = "SELECT C.ID, C.TEXT FROM NEWS_COMMENT N, COMMENT C "
			+ " WHERE N.COMMENTS_ID = C.ID AND N.NEWS_ID = ?1 ORDER BY C.ID DESC", nativeQuery = true)
	List<Comment> findCommentsByNewsId(Long newsId);

}
