package by.itsupportme.news.repositories;

import by.itsupportme.news.model.News;

public interface NewsRepository extends BaseRepository<News, Long> {

}
