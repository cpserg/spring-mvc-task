package by.itsupportme.news.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Base repository definitions. Contains generic methods for all repositories.
 * The methods describes in
 * {@link org.springframework.data.jpa.repository.JpaRepository}
 * 
 * @author Siarhei Tsytrykau
 *
 * @param <T> Domain class type
 * @param <ID> ID type
 */
@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {

}
