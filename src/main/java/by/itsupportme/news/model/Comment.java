package by.itsupportme.news.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The class describes Comment entity. The every comment contains a text of the
 * discuss about a certain news
 * 
 * @author Siarhei Tsytrykau
 *
 */
@Entity
@Table
public class Comment extends BaseEntity {
	@Column
	private String text;

	public Comment() {
	}

	public Comment(String text) {
		this.text = text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}

	@Override
	public String toString() {
		return "Comment [text=" + text + "]";
	}

}
