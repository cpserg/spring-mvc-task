package by.itsupportme.news.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * A base class for all entities. This class contains shared information,
 * required for all entities.
 * 
 * @author Siarhei Tsytrykau
 *
 */
@MappedSuperclass
public abstract class BaseEntity {
	/**
	 * Entity's identifier.
	 * 
	 * @see <a href="https://en.wikipedia.org/wiki/Unique_key">id</a>
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
