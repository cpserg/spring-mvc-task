package by.itsupportme.news.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The class describes News entity. News contains title, text and list of
 * {@link by.itsupportme.news.model.Comment}
 * 
 * @author Siarhei Tsytrykau
 *
 */
@Entity
@Table
public class News extends BaseEntity {
	private static final int TEXT_MAX_LENGTH = 4096;

	@Column
	private String title;
	@Column(length = TEXT_MAX_LENGTH)
	private String text;
	@Column
	@Temporal(TemporalType.DATE)
	private Date date;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Comment> comments = new ArrayList<Comment>();

	public News() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	/**
	 * Returns a string representation of the News.
	 */
	@Override
	public String toString() {
		return "News [id=" + this.getId() + ", title=" + title + ", text=" + text + ", date=" + date + ", comments="
				+ comments + "]";
	}

}
