package by.itsupportme.news.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import by.itsupportme.news.model.Comment;
import by.itsupportme.news.model.News;
import by.itsupportme.news.web.service.CommentService;
import by.itsupportme.news.web.service.NewsService;

/**
 * This class describes RestController
 * 
 * @author Siarhei Tsytrykau
 * @see {@link https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#mvc-controller}
 *
 */
@RestController
public class AjaxController {
	private final static String JSON_HEADER = "application/json; charset=UTF-8";

	@Autowired
	private NewsService newsService;

	@Autowired
	private CommentService commentService;

	/**
	 * Retrieves all Comments for a News by news's id and send it as response body
	 * 
	 * @param id id news
	 * @return {@link java.util.List} of Comment
	 */
	@GetMapping("/comment/get/{id}")
	@ResponseBody
	public List<Comment> getComments(@PathVariable Long id) {
		return commentService.findCommentsByNewsId(id);
	}

	/**
	 * This method creates a new Comment for a News and send list of comments as response body
	 * 
	 * @param id News's ID
	 * @param comment Comment JSON
	 * @return {@link java.util.List} of Comment for this News
	 */
	@PostMapping(value = "/comment/create/{id}", produces = JSON_HEADER)
	public @ResponseBody List<Comment> createComment(@PathVariable Long id, @RequestBody Comment comment) {
		return newsService.addCommentToNews(id, comment);
	}
	
	/**
	 * This method updates (or adds) a News by news's id and send changed (or updated) News as response body.
	 * 
	 * @param news News JSON
	 * @return News JSON
	 */
	@PostMapping(value = "/news/save", produces = JSON_HEADER)
	public  @ResponseBody News updateNews(@RequestBody News news) {
		return newsService.saveAndFlush(news);
	}

}
