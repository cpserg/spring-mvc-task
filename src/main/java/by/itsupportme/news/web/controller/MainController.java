package by.itsupportme.news.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import by.itsupportme.news.model.News;
import by.itsupportme.news.web.service.NewsService;

/**
 * This class describes Controller
 * 
 * @author Siarhei Tsytrykau
 * @see {@link https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#mvc-controller}
 *
 */
@Controller
public class MainController {
	private final static int NEWS_PER_PAGE = 5;
	private final static int FIRST_PAGE_NUMBER = 0;

	@Autowired
	private NewsService newsService;

	/**
	 * This method opens main page of Web application with a news list and a pagination.
	 * 
	 * @param pageable Implementation of default pagination
	 * @return View's name
	 */
	@GetMapping("/")
	public String viewNewsList (Model model, @PageableDefault(size = NEWS_PER_PAGE, page = FIRST_PAGE_NUMBER, sort = {"id"}, 
			direction = Direction.DESC) Pageable pageable) {
		Page<News> page = newsService.findAll(pageable);
		model.addAttribute("news", page.getContent());
		model.addAttribute("page", page);
		return "news-list";
	}

	/**
	 * This method shows a News for view and for add comments.
	 * 
	 * @param news News's id
	 * @return view's name
	 */
	@GetMapping("/news/view/{id}")
	public String viewNews(@PathVariable("id") News news, Model model) {
		model.addAttribute("news", news);
		return "news-view";
	}

	/**
	 * This method designed for edit (or add) a News.
	 * 
	 * @param news news's id (or 0 for new news)
	 * @return view's name
	 */
	@GetMapping("/news/edit/{id}")
	public String editNews(@PathVariable("id") News news, Model model) {
		model.addAttribute("news", news);
		return "news-add-edit";
	}

}
