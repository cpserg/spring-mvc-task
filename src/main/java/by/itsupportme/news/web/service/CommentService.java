package by.itsupportme.news.web.service;

import java.util.List;

import by.itsupportme.news.model.Comment;

/**
 * This class describes service for Comment.
 * 
 * 
 * @author Siarhei Tsytrykau
 *
 */
public interface CommentService extends BaseService<Comment> {

	/**
	 * Returns a list of Comment for certain News by specified newsId
	 * 
	 * @param newsId ID of News
	 * @return {@link java.util.List} of Comment
	 * @see by.itsupportme.news.repositories.CommentRepository#findCommentsByNewsId()
	 * @see by.itsupportme.news.model.Comment
	 * @see by.itsupportme.news.model.News
	 */
	List<Comment> findCommentsByNewsId(Long newsId);

}
