package by.itsupportme.news.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.itsupportme.news.model.Comment;
import by.itsupportme.news.repositories.CommentRepository;

/**
 * Implementations of Comment service.
 * 
 * @author Siarhei Tsytrykau
 *
 */
@Service
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentRepository commentRepository;

	/**
	 * Saves the Comment and flushes changes instantly
	 */
	@Override
	public Comment saveAndFlush(Comment comment) {
		return commentRepository.saveAndFlush(comment);
	}

	/**
	 * Returns a list of Comment for certain News by specified newsId
	 */
	@Override
	public List<Comment> findCommentsByNewsId(Long newsId) {
		return commentRepository.findCommentsByNewsId(newsId);
	}
}
