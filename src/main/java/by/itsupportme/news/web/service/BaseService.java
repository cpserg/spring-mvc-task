package by.itsupportme.news.web.service;

import by.itsupportme.news.model.BaseEntity;

/**
 * 
 * This class describes generic methods for all services.
 * 
 * @author Siarhei Tsytrykau
 * @param <T> Domain class type
 */
public interface BaseService<T extends BaseEntity> {

	/**
	 * Saves a given entity and flushes changes instantly.
	 * 
	 * @param entity Entity instance
	 * @return Entity instance
	 * @see org.springframework.data.jpa.repository.JpaRepository#saveAndFlush()
	 */
	T saveAndFlush(T entity);

}
