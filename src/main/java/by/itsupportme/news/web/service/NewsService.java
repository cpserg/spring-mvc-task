package by.itsupportme.news.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import by.itsupportme.news.model.Comment;
import by.itsupportme.news.model.News;

/**
 * This class describes service for News.
 * 
 * @author Siarhei Tsytrykau
 *
 */
public interface NewsService extends BaseService<News> {

	/**
	 * Add a comment to the News
	 * 
	 * @param newsId Long ID of News
	 * @param comment Comment instance
	 * @return Comment instance
	 * @see by.itsupportme.news.model.Comment
	 * @see by.itsupportme.news.model.News
	 */
	List<Comment> addCommentToNews(Long newsId, Comment comment);

	/**
	 * Returns a {@link Page} of News meeting the paging restriction provided in the
	 * {@code Pageable} object.
	 * 
	 * @param pageable Instance of Pageable interface
	 * @return {@link Page}
	 */
	Page<News> findAll(Pageable pageable);

}
