package by.itsupportme.news.web.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import by.itsupportme.news.model.Comment;
import by.itsupportme.news.model.News;
import by.itsupportme.news.repositories.CommentRepository;
import by.itsupportme.news.repositories.NewsRepository;

/**
 * Implementations of News service.
 * 
 * @author Siarhei Tsytrykau
 *
 */
@Service
public class NewsServiceImpl implements NewsService {

	@Autowired
	private NewsRepository newsRepository;

	@Autowired
	private CommentRepository commentRepository;

	/**
	 * Saves the News instance and flushes changes instantly.
	 * 
	 */
	@Override
	public News saveAndFlush(News news) {
		System.out.println("SAVE: " + news);
		return newsRepository.saveAndFlush(news);
	}

	/**
	 * Returns a {@link Page} of News meeting the paging restriction provided in the
	 * {@code Pageable} object.
	 */
	@Override
	public Page<News> findAll(Pageable pageable) {
		Page<News> page = newsRepository.findAll(pageable);
		return page;
	}

	/**
	 * Add a comment to a News
	 */
	@Transactional
	@Override
	public List<Comment> addCommentToNews(Long newsId, Comment comment) {
		News news = newsRepository.findOne(newsId);
		news.getComments().add(comment);
		newsRepository.saveAndFlush(news);
		return commentRepository.findCommentsByNewsId(newsId);
	}
}
