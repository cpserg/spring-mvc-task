package by.itsupportme.news.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.servlet.config.annotation.DelegatingWebMvcConfiguration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

/**
 * DispatcherServlet Configuration
 * 
 * @author Siarhei Tsytrykau
 *
 */
@Configuration
@ComponentScan("by.itsupportme.news.web")
@EnableSpringDataWebSupport
public class DispatcherConfig extends DelegatingWebMvcConfiguration {

	/**
	 * Register JSP {@link org.springframework.web.servlet.ViewResolver
	 * ViewResolver} with the specified prefix and suffix.
	 * 
	 */
	@Override
	protected void configureViewResolvers(ViewResolverRegistry registry) {
		registry.jsp("/WEB-INF/views/", ".jsp");
		registry.enableContentNegotiation(new MappingJackson2JsonView());
	}

	/**
	 * Add a resource handler for serving static resources based on the specified
	 * URL path patterns.
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

}
