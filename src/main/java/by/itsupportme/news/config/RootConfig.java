package by.itsupportme.news.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Database and Hibernate Configuration
 * 
 * @author Siarhei Tsytrykau
 *
 */
@Configuration
@EnableJpaRepositories("by.itsupportme.news.repositories")
@EnableTransactionManagement
@PropertySource("classpath:db.properties")
@PropertySource("classpath:hibernate.properties")
public class RootConfig {

	@Autowired
	private Environment env;

	/**
	 * Configure and instantiation a DataSource bean.
	 * 
	 * @return DataSource instance
	 */
	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(env.getRequiredProperty("db.driverClassName"));
		dataSource.setUrl(env.getRequiredProperty("db.url"));
		dataSource.setUsername(env.getRequiredProperty("db.username"));
		dataSource.setPassword(env.getProperty("db.password"));
		dataSource.setInitialSize(env.getProperty("db.initialSize", Integer.TYPE));
		dataSource.setMaxTotal(env.getProperty("db.maxTotal", Integer.TYPE));
		dataSource.setMaxIdle(env.getProperty("db.maxIdle", Integer.TYPE));
		dataSource.setMinIdle(env.getProperty("db.minIdle", Integer.TYPE));
		return dataSource;
	}

	/**
	 * Configure and instantiation a EntityManagerFactory bean.
	 * 
	 * @return EntityManagerFactory
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(env.getProperty("hibernate.generate_ddl", Boolean.TYPE));
		vendorAdapter.setShowSql(env.getProperty("hibernate.show_sql", Boolean.TYPE));
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan(env.getRequiredProperty("hibernate.entity.package"));
		factory.setDataSource(dataSource());
		return factory;
	}

	/**
	 * Create a new JpaTransactionManager.
	 * 
	 * @return
	 */
	@Bean
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return txManager;
	}

}
