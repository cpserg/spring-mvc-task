package by.itsupportme.news;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import by.itsupportme.news.config.DispatcherConfig;
import by.itsupportme.news.config.RootConfig;

/**
 * This class performs register and initialization the DispatcherServlet. It is
 * auto-detected by the Servlet container
 *
 * @author Siarhei Tsytrykau
 *
 */
public class NewsInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	private static final String DISPATCHER_URL = "/";

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { RootConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { DispatcherConfig.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { DISPATCHER_URL };
	}

}
