<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ include file="/WEB-INF/views/header.jsp"%>

<div class="news">
  <article class="news-view">
    <header>
      <h1 class="news-view__title"><c:out value="${news.title}"/></h1>
      <p>
        <time class="news-view__date" datetime="${news.date}">
          <f:formatDate type = "date" pattern="y-M-d" value = "${newsitem.date}" />
        </time>
      </p>
    </header>
    <p class="news-view__text"><c:out value="${news.text}"/></p>
    <div class="news-form">
      <input id="comment-text" data-news-id="${news.id}" type="text" placeholder="Enter a comment here..." />
      <input id="comment-button" type="button" value="Add comment">
    </div>
    <section class="comments">
      <h2 class="comments__title">Comments</h2>
      <div id="comments-ajax">
      </div>
    </section>
  </article>
</div>

<div id="root-url" data-root-url="<c:url value='/' />"></div>
<script>
	var rootUrl = document.getElementById("root-url").dataset.rootUrl;
</script>
<script src="<c:url value = "/resources/js/Xhr.js"/>"  ></script>
<script src="<c:url value = "/resources/js/Comment.js"/>" ></script>
<script src="<c:url value = "/resources/js/Template.js"/>" ></script>
<script src="<c:url value = '/resources/js/commentService.js'/>" ></script>

<%@ include file="/WEB-INF/views/footer.jsp"%>
