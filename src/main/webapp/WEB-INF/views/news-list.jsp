<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ include file="/WEB-INF/views/header.jsp"%>

<div>
  <c:forEach items="${news}" var="newsitem">
    <article data-id="${newsitem.id}">
      <header>
        <h1><c:out value="${newsitem.title}"/></h1>
        <p>
          <time datetime="${newsitem.date}">
            <f:formatDate type = "date" pattern="y-M-d" value = "${newsitem.date}" />
          </time>
        </p>
      </header>
      <p class="news-text short"><c:out value="${newsitem.text}"/></p>
      <div class="news-item-links menu">
        <a href="<c:url value="/news/view/${newsitem.id}" />">View</a>
        <a href="<c:url value="/news/edit/${newsitem.id}" />">Edit</a>
      </div>
    </article>
  </c:forEach>

  <div class="pagination menu">
    <c:forEach var="current" begin="0" end="${page.totalPages - 1}">
      <a href="<c:url value='/?page=${current}&size=${page.size}'/>" <c:if test="${page.number == current}">class="active"</c:if> > 
        ${current + 1}
      </a>
    </c:forEach>        
  </div>
</div>

<%@ include file="/WEB-INF/views/footer.jsp"%>
