<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
  <head>
    <title>Spring MVC Simple Task</title>
    <meta charset="UTF-8">
    <meta name="author" content="Siarhei Tsytrykau">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
  </head>
  <body class="content">
    <nav class="main-menu menu">
      <a class="home-page-ref" href="<c:url value="/" />">Home page</a>
      <a href="<c:url value="/news/edit/0" />">Add news</a>
    </nav>
