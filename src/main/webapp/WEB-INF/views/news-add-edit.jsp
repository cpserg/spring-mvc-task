<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ include file="/WEB-INF/views/header.jsp"%>

<div class="news">
  <form id="news-form" class="news-form" data-news-id="${news.id}">
    <label for="news-title">Title</label>
    <input id="news-title" type="text" name="title" maxlength="255" value="<c:out value='${news.title}'/>">

    <label for="news-date">Date</label>
    <input id="news-date" type="date" name="date" value="<f:formatDate type='date' pattern='y-M-d' value = '${news.date}' />"/>

    <label for="news-text">Text</label>
    <textarea id="news-text" name="text" rows="8" cols="80" spellcheck="false"><c:out value='${news.text}'/></textarea>

    <div id="button-ajax">
      <input id="save-news" type="button" name="button" value="Save">
    </div>
  </form>
</div>

<div id="root-url" data-root-url="<c:url value='/' />"></div>
<script>
	var rootUrl = document.getElementById("root-url").dataset.rootUrl;
</script>
<script src="<c:url value = "/resources/js/Xhr.js"/>"  ></script>
<script src="<c:url value = "/resources/js/News.js"/>" ></script>
<script src="<c:url value = "/resources/js/Template.js"/>" ></script>
<script src="<c:url value = '/resources/js/newsService.js'/>" ></script>

<%@ include file="/WEB-INF/views/footer.jsp"%>
