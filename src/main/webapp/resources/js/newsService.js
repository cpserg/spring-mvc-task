console.log("Load newsService.js");
/* ELEMENT DEFINITION */
var newsId = document.getElementById("news-form").dataset.newsId;
var newsTitle = document.getElementById("news-title");
var newsDate = document.getElementById("news-date");
var newsText = document.getElementById("news-text");

/* SET CLICK EVENT */
document.getElementById("save-news").onclick = saveNews;

/* FUNCTIONS DECLARATION */
function saveNews() {
  var news = new News(newsTitle.value, newsText.value, newsDate.value, newsId);
  news.save(changeButtons);
}

function changeButtons(response) {
  document.getElementById("button-ajax").innerHTML = Template.newsButtons(response);
};
