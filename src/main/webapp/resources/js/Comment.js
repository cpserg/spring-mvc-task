console.log("Load Comment.js");

function Comment(text, id = 0) {
  this.id = id;
  this.text = text;
  this.url = window["rootUrl"] + "comment";
}

Comment.prototype = Object.create(Xhr.prototype);
Comment.prototype.constructor = Comment;

Comment.prototype.create = function(params, callback) {
  var method = "POST";
  var url =  this.url + "/create/" + params.newsId;
  var body = {"id" : this.id, "text" : this.text};
  this.load({method: method, url: url, body: body}, callback);
}

Comment.prototype.get = function(params, callback) {
  var method = "GET";
  var url = this.url + "/get/" + params.newsId;
  this.load({method: method, url: url}, callback);
}
