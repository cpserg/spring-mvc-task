console.log("Load Xhr.js");

function Xhr() {

}

Xhr.prototype.load = function(queryParam, callback) {
  var method = queryParam.method;
  var url = queryParam.url;
  var asyn = true;
  var body = queryParam.body || null;
  var xhr = new XMLHttpRequest();
  xhr.open(method, url, asyn);

  if (body) {
    body = JSON.stringify(queryParam.body);
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
  }

  xhr.onreadystatechange = function() {
    if (xhr.readyState != 4) return;
    if (xhr.status != 200) {
      console.log("ERROR: " + xhr.status + " " + xhr.statusText + " " + xhr.responseURL);
      return;
    }
    if (callback) {
      callback.call(this, JSON.parse(xhr.responseText));
    }
    console.log("THE REQUEST IS COMPLETE: " + xhr.status + " " + xhr.statusText);
  }

  xhr.send(body);
  console.log(method + " " + url + " " + asyn + " " + body);
}
