console.log("Load Template.js");

function Template() {
}

Template.comment = comment => `
  <article class="comment">
    <p class="comment__text">${comment.text}</p>
  </article>
`;

Template.newsButtons = news => `
  <input id="view-news" type="button" value="View"
    onclick="document.location.href = '${this.rootUrl}news/view/${news.id}'">
  <input id="edit-news" type="button" value="Edit"
    onclick="document.location.href = '${this.rootUrl}news/edit/${news.id}'">
`;
