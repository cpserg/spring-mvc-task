console.log("Load commentService.js");
/* ELEMENT DEFINITION */
var commentText = document.getElementById("comment-text");

/* GET COMMENTS BY XHR */
document.getElementById("comments-ajax").innerHTML = loadComments();

/* SET CLICK EVENT */
document.getElementById("comment-button").onclick = createComment;

/* FUNCTIONS DECLARATION */
function loadComments(){
  var comment = new Comment();
  comment.get({newsId: commentText.dataset.newsId}, refreshComments);
}
function createComment() {
  if (commentText.value) {
    var comment = new Comment(commentText.value);
    comment.create({newsId: commentText.dataset.newsId}, refreshComments);
    commentText.value = "";
  }
};
function refreshComments(response) {
  document.getElementById("comments-ajax").innerHTML = response.map(Template.comment).join("");
};
