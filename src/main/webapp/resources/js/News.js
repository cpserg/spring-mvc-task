console.log("Load News.js");

function News(title, text, date, id = 0) {
  this.id = id;
  this.title = title;
  this.text = text;
  this.date = date;
  this.comments = [];
  this.url = window["rootUrl"] + "news";
};

News.prototype = Object.create(Xhr.prototype);
News.prototype.constructor = News;

News.prototype.save = function(callback) {
  var method = "POST";
  var methodUrl = "/save";
  this.load(this.buildParams(method, methodUrl), callback);
};

News.prototype.buildParams = function(method, methodUrl) {
  var url = this.url + methodUrl;
  var body = {
    id: this.id,
    title: this.title,
    text: this.text,
    date: this.date,
    comments: this.comments
  };
  return {method: method, url: url, body: body};
}
